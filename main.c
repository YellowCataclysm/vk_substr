#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define ALPHABET_POWER 95
#define FIRST_CHAR_OFFSET 32
#define CHAR_TO_INDEX(c) (((int)c - FIRST_CHAR_OFFSET))

// 32 - 127


int * genTable(const char * const needle );
char * getFile( char * path );
int BMHSearch(const char * const needle , const char * const text , long text_size );
char * getInput();

int main(int argc , char ** argv)
{
    // Getting file
    FILE * fp = fopen(argv[1],"r");
    if( fp == NULL )
    {
        // FILE NOT FOUND
        return -1;
    }

    // Getting file size
    struct stat st;
    fstat( fileno(fp) , &st );
    unsigned long fsize = st.st_size;

    char * filedata = malloc( sizeof(char) * fsize + 1 );
    filedata[fsize] = 0;

    fread( filedata , sizeof(char) , fsize , fp );

    //printf( "FILE : %s\n" , filedata );

    char * input = NULL;
    while( (input = getInput()) != NULL )
    {
        if( BMHSearch(input,filedata,fsize) != -1 ) printf("YES\n");
        else printf("NO\n");
        free(input);
        input = NULL;
    }
    free(filedata);
    fclose(fp);
    return 0;
}

int BMHSearch( const char * const needle , const char * const text , long text_size )
{
    size_t needle_size = strlen(needle);
    int * offset_table = genTable( needle );
    register long needle_index = 0;
    register long text_index = 0;
    while( text_index < text_size )
    {
        needle_index = 0;
        while( (needle_index < needle_size) && ( needle[needle_index] == text[ text_index + needle_index ] ) )
            needle_index++;

        if( needle_index != needle_size )
        {
            text_index += offset_table[ CHAR_TO_INDEX( text[text_index + needle_size - 1 ] ) ];
        }
        else return text_index;
    }
    free( offset_table );
    return -1;
}

char * getInput()
{
    printf( "Enter pattern: " );
    char * input = NULL;
    size_t len = 0;
    ssize_t read = 0;
    if( (read = getline(&input , &len , stdin )) != -1 )
    {
        if( !strncmp( input , "exit" , 4 ) )    // exit string passed
            return NULL;

        input[read-1]='\0';
        return input;
    }
    int errcode = ferror(stdin);
    printf( "Bad input.\n\tError code: %d \n\tError string: %s\n" , errcode , strerror( errcode ) );
    return NULL;
}

int * genTable( const char * const needle )
{
    int * table = malloc( sizeof(int) * ALPHABET_POWER );
    int needle_len = strlen(needle);
    memset( table , 0 , sizeof(int) * ALPHABET_POWER );
    register int i = 0;
    for( i = 0 ; i < ALPHABET_POWER ; i++ ) table[i] = needle_len;
    for( i = 0 ; i < needle_len - 1 ; i++ ) table[ CHAR_TO_INDEX( needle[i] ) ] = needle_len - 1 - i;
    return table;
}

char * getFile( char * path )
{
    FILE * fd = fopen(path,"r");
    if( fd == NULL ) return NULL;
    struct stat st;
    fstat( fileno(fd) , &st );
    int fsize = st.st_size;
    char * filedata = malloc( sizeof(char) * fsize + 1);
    filedata[fsize] = 0;
    return filedata;
}

